# RBTray

RBTray is a small Windows program that runs in the background and allows almost any window to be minimized to the system tray by:

- Right-Clicking its minimize button
- Shift-Right-Clicking on its title bar
- Using the Windows-Alt-Down hotkey

Note that not all of these methods will work for every window, so please use whichever one works for your needs.

RBTray is free, open source, and is distributed under the terms of the [GNU General Public Licence](http://www.gnu.org/copyleft/gpl.html).


## Download
- [64 bit binaries](https://bitbucket.org/davidksouthgate/rbtray/downloads/rbtray-4.10-windows-x64.zip)
- [32 bit binaries](https://bitbucket.org/davidksouthgate/rbtray/downloads/rbtray-4.10-windows-x86.zip)
- [Original RBTray](http://sourceforge.net/projects/rbtray/files/)

## Installing

Download either the 32-bit or 64-bit binaries (depending on your OS) to a folder,
for example "C:\Program Files\RBTray".  Double click RBTray.exe to start it.  If
you want it to automatically start after you reboot, create a shortcut to RBTray.exe
in your Start menu's Startup group.

## Building
Building is mostly straight forward. The project is currently using platform toolset v141 (Visual Studio 2017).

### Microsoft Visual Studio 2019
The workload "Desktop development with C++" needs to be installed with the following optional components:
* C++ ATL for latest v142 build tools (x86 & x64)
* C++ MFC for latest v142 build tools (x86 & x64)
* MSVC v141 - VS 2017 C++ x64/x86 build tools

Select from "Individual Components"
* C++ ATL for v141 build tools (x86 & x64)
* C++ MFC for v141 build tools (x86 & x64)

### Microsoft Visual Studio 2017
The workload "Desktop development with C++" needs to be installed with the following optional components (these may have different names in older versions of VS 2017):
* Visual C++ ATL for x86 and x64
* Visual C++ MFC for x86 and x6

## Using

To minimize a program to the system tray, you can use any of these methods:

- Right-click with the mouse on the program's minimize button.
- Hold the Shift key while Right-clicking on the program's title bar.
- Pressing Windows-Alt-Down on the keyboard (all at the same time).

This should create an icon for the window in the system tray. To restore the
program's window, single-click the program's icon in the tray. Alternatively,
you can Right-click on the tray icon which should bring up a popup menu, then
select Restore Window.

## Exiting

Right click on any tray icon created by RBTray and click Exit RBTray in the
popup menu, or run RBTray.exe with the `--exit` parameter.

## Other

For original forum, bug tracker, etc. see [RBTray SourceForge project page](http://sourceforge.net/projects/rbtray/).

Copyright ©️ 1998-2011 Nikolay Redko, J.D. Purcell

Copyright ©️ 2015 Benbuck Nason
